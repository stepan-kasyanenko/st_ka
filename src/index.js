import './assets/styles/colors.scss';
import './assets/styles/style.scss';
import './app/module';
import './app/icons';

import MainComponent from './app/components/main/main.component';

var myApp = new MainComponent(document.body);
