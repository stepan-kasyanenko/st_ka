import pin from '../assets/images/ionicicon/_ionicons_svg_ios-pin.svg';
import call from '../assets/images/ionicicon/_ionicons_svg_ios-call.svg';
import camera from '../assets/images/ionicicon/_ionicons_svg_ios-camera.svg';
import addCircle from '../assets/images/ionicicon/_ionicons_svg_ios-add-circle.svg';
import star from '../assets/images/ionicicon/_ionicons_svg_ios-star.svg';
import home from '../assets/images/ionicicon/_ionicons_svg_ios-home.svg';
import globe from '../assets/images/ionicicon/_ionicons_svg_md-globe.svg';
import create from '../assets/images/ionicicon/_ionicons_svg_md-create.svg';
import refresh from '../assets/images/ionicicon/_ionicons_svg_md-refresh.svg';

export var ICONS = {
    pin: pin,
    call: call,
    camera: camera,
    addCircle: addCircle,
    star: star,
    home: home,
    globe: globe,
    create: create,
    refresh: refresh,
};