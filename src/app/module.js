import ButtonComponent from "./components/common/button/button.component";
import PanelComponent from "./components/common/panel/panel.component";
import ProfileComponent from "./components/profile/profile.component";
import ProfileHeaderComponent from "./components/profile/profile-header/profile-header.component";
import ProfileAvatarComponent from "./components/profile/profile-header/profile-avatar/profile-avatar.component";
import ProfileBioComponent from "./components/profile/profile-header/profile-bio/profile-bio.component";
import IconComponent from "./components/common/icon/icon.component";
import IconTextComponent from "./components/common/icon-text/icon-text.component";
import TabsComponent from "./components/common/tabs/tabs.component";
import TabComponent from "./components/common/tabs/tab/tab.component";
import FollowComponent from "./components/follow/follow.component";
import ReviewComponent from "./components/review/review.component";
import TabContentComponent from "./components/common/tabs/tab-content/tab-content.component";
import ProfileAboutComponent from "./components/profile/profile-about/profile-about.component";
import ShowEditComponent from "./components/common/show-edit/show-edit.component";
import PopupDirective from "./components/common/popup/popup.directive";
import InputDirective from "./components/common/input/input.directive";
import LoaderComponent from "./components/common/loader/loader.component";
import UploadButtonComponent from "./components/common/upload-button/upload-button.component";
import ProfileAboutEditComponent from "./components/profile/profile-about/profile-about-edit/profile-about-edit.component";

export var REGISTER = {
    ButtonComponent: ButtonComponent,
    PanelComponent: PanelComponent,
    ProfileComponent: ProfileComponent,
    ProfileHeaderComponent: ProfileHeaderComponent,
    ProfileAvatarComponent: ProfileAvatarComponent,
    ProfileBioComponent: ProfileBioComponent,
    IconComponent: IconComponent,
    IconTextComponent: IconTextComponent,
    TabsComponent: TabsComponent,
    TabComponent: TabComponent,
    TabContentComponent: TabContentComponent,
    FollowComponent: FollowComponent,
    ReviewComponent: ReviewComponent,
    ProfileAboutComponent: ProfileAboutComponent,
    ShowEditComponent: ShowEditComponent,
    PopupDirective: PopupDirective,
    InputDirective: InputDirective,
    LoaderComponent: LoaderComponent,
    UploadButtonComponent: UploadButtonComponent,
    ProfileAboutEditComponent: ProfileAboutEditComponent
};