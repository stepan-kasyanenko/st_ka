import './main.component.scss';
import './main-mobile.component.scss';
import BaseUI from '../common/base/base-ui.component';
import content from './main.component.html';


export default class MainComponent extends BaseUI {
    constructor (parentNode) {
        super({parentNode:parentNode,content:content});
    }
}

