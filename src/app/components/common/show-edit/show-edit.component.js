import './show-edit.component.scss';
import './show-edit-mobile.component.scss';
import BaseUI from '../base/base-ui.component';
import content from './show-edit.component.html'
import {EventEmitter} from "../event-emitter-property/EventEmitterProperty";

export default class ShowEditComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
        this.onSaveEvent = new EventEmitter();
    }

    init () {
        this.icon = this.data.icon;
        this.stroke = this.data.stroke;
        this.text = this.data.text;
        this.label = this.data.label;
        this.$popupComponent.$inputComponent.value = " ";
        this.$popupComponent.$form.addEventListener("submit", (event) => this.onSave(event));
        this.$popupComponent.$btnCancelComponent.element.addEventListener("click", (event) => this.onCancel(event));
    }

    set text (text) {
        this._text = text === undefined ? "" : text;
        this.$text.textContent = this._text;
    }

    get text () {
        return this._text;
    }

    set icon (icon) {
        this.$iconComponent.icon = icon;
    }

    set stroke (val) {
        this.$iconComponent.stroke = val;
    }

    set label (val) {
        this._label = val === undefined ? "" : val;
        this.$text.title = this._label;
    }

    get label () {
        return this._label;
    }

    onSave (event) {
        event.preventDefault();
        this.onSaveEvent.next(this.$popupComponent.$inputComponent.value);
        this.hidePopup();
    }

    onCancel (event) {
        event.preventDefault();
        this.hidePopup();
    }

    showPopup () {
        this.$popupComponent.$inputComponent.value = this.text;
        this.$popupComponent.$inputComponent.label = this.label;
        this.$popupComponent.show();
    }
    hidePopup () {
        this.$popupComponent.hide();
    }
}

