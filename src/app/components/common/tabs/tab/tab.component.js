import './tab.component.scss';
import './tab-mobile.component.scss';
import BaseUI from '../../base/base-ui.component';
import content from './tab.component.html'

export default class TabComponent extends BaseUI {
    constructor (options) {
        var temp =document.createElement('div');
        temp.innerHTML = content;
        temp.firstChild.firstChild.innerHTML = options.content || (options.element &&options.element.innerHTML) || "";
        options.content = temp.innerHTML;
        super(options);
    }
}

