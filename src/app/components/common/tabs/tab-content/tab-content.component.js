import './tab-content.component.scss';
import BaseUI from '../../base/base-ui.component';
import content from './tab-content.component.html'

export default class TabContentComponent extends BaseUI {
    constructor (options) {
        const temp =document.createElement('div');
        temp.innerHTML = content;
        temp.firstChild.innerHTML = options.content || (options.element &&options.element.innerHTML) || "";
        options.content = temp.innerHTML;
        super(options);
    }
}

