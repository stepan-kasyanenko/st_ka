import {EventEmitterProperty} from "../event-emitter-property/EventEmitterProperty";

export class TabsService {

    constructor (current){
        this.current = new EventEmitterProperty(current || 0);
    }
}