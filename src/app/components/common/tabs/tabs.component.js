import './tabs.component.scss';
import './tabs-mobile.component.scss';
import BaseUI from '../base/base-ui.component';
import content from './tabs.component.html'
import {TabsService} from "./tabs.service";
import TabContentComponent from "./tab-content/tab-content.component";
import TabComponent from "./tab/tab.component";

export default class TabsComponent extends BaseUI {
    constructor (options) {
        const temp = document.createElement('div');
        temp.innerHTML = content;
        const tabs = temp.firstChild.children[0];
        const _content = temp.firstChild.children[2];

        const outerTemp = document.createElement('div');
        outerTemp.innerHTML = options.content || (options.element && options.element.innerHTML) || "";
        [].slice.call(outerTemp.querySelectorAll('[data-component="TabComponent"]')).forEach((elem, i) => {
            tabs.appendChild(elem);
            elem.setAttribute('st-click', 'setCurrentTab(' + i + ')')
        });

        [].slice.call(outerTemp.querySelectorAll('[data-component="TabContentComponent"]')).forEach(elem => {
            _content.appendChild(elem);
        });

        options.content = temp.innerHTML;
        super(options);
    }

    init () {
        this.tabsService = new TabsService(this.data.current);
        this.setCurrentTab(this.tabsService.current.value);
        this.calculateTabsWidth();
            this.checkSize();
        setTimeout(()=>{
            this.checkSize();
        },1);
        let timeoutId = null;
        window.addEventListener('resize', () => {
            if (timeoutId)
                clearTimeout(timeoutId);
            timeoutId = setTimeout(() => {
                this.checkSize();
                timeoutId = null;
            }, 10);
        });
    }

    calculateTabsWidth () {
        this.tabsWidth = 38;
        this.components.filter(f => f instanceof TabComponent)
          .forEach((comp) => {
              this.tabsWidth += comp.element.offsetWidth;
          });
    }

    setCurrentTab (current) {
        this.tabsService.current.value = current;
        this.components.filter(f => f instanceof TabComponent)
          .forEach((comp, i) => {
              if (i === this.tabsService.current.value)
                  comp.element.classList.add("tabs__tab--active");
              else
                  comp.element.classList.remove("tabs__tab--active");
          });
        this.components.filter(f => f instanceof TabContentComponent)
          .forEach((comp, i) => {
              if (i === this.tabsService.current.value)
                  comp.element.classList.add("tabs__tab__content--active");
              else
                  comp.element.classList.remove("tabs__tab__content--active");
          });
    }

    checkSize () {

        this.$showMoreContainer.classList.add('tabs__show-more--hide');
        let needShowMore = false;
        var tabsWidth = this.tabsWidth;
        let tabs = this.components.filter(f => f instanceof TabComponent)
          .map((comp) => {
              this.$tabsContainer.appendChild(comp.element);
              return comp;
          });
        for (let i = tabs.length - 1; i >= 0; i--) {
            if (tabsWidth > this.$tabsContainer.offsetWidth) {
                tabsWidth -= tabs[i].element.offsetWidth;
                this.$popupComponent.element.insertBefore(tabs[i].element, this.$popupComponent.element.firstElementChild);
                needShowMore = true;
            }
            else
                break;
        }
        if (needShowMore)
            this.$showMoreContainer.classList.remove('tabs__show-more--hide');
        else
            this.$popupComponent.hide();
        this.$tabsContainer.insertBefore(this.$showMoreContainer, null);
    }

    showMoreMenu () {
        this.$popupComponent.show();
    }
}

