import './button.component.scss';
import './button-mobile.component.scss';
import BaseUI from '../base/base-ui.component';
import content from './button.component.html';

export default class ButtonComponent extends BaseUI {
    constructor (options) {
        var temp = document.createElement('div');
        temp.innerHTML = content;
        temp.firstChild.innerHTML = options.content || (options.element && options.element.innerHTML) || "";
        options.content = temp.innerHTML;
        super(options);
    }
}


