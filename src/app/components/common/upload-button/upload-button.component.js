import './upload-button.component.scss';
import './upload-button-mobile.component.scss';
import content from './upload-button.component.html';
import BaseUI from "../base/base-ui.component";


export default class UploadButtonComponent extends BaseUI {
    constructor (options) {
        const temp = document.createElement('div');
        temp.innerHTML = content;
        options.content = temp.innerHTML;
        super(options);
    }

    init(){
        this.text = this.data.text;
    }

    set text(value){
        this._text = value;
        this.element.querySelector('.upload-button__text').innerHTML = value;
    }
    get text(){
       return this._text;
    }

}
