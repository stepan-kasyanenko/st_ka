import './input.directive.scss';
import './input-mobile.directive.scss';
import BaseUI from '../base/base-ui.component';
import content from './input.directive.html'

export default class InputDirective extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
    }

    init () {
        this.value = this.data.value;
        this.label = this.data.label;
        this.$input.addEventListener("input",()=>{this.onInput()});
    }

    onInput(){
        const val = this.$input.value;
        if(val === undefined  || val==="")
            this.element.classList.remove("material-input--has-value");
        else
            this.element.classList.add("material-input--has-value");
    }

    set value (val) {
        this.$input.value = val === undefined ? "" : val;
        if(val === undefined  || val==="")
            this.element.classList.remove("material-input--has-value");
        else
            this.element.classList.add("material-input--has-value");
    }

    get value () {
        return this.$input.value;
    }

    set label (val) {
        this.$label.textContent = val || "";
    }

    get label () {
        return this.$label.textContent;
    }
}

