import './icon.component.scss';
import content from './icon.component.html';
import BaseUI from "../base/base-ui.component";
import {ICONS} from "../../../icons";


export default class IconComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
    }

    init () {
        this.icon = this.data.icon;
        this.stroke = this.data.stroke;
    }

    set icon (value) {
        this._icon = value;
        if (ICONS[value]) {
            this.element.classList.remove("hidden");
            this.element.querySelector('.icon__icon').innerHTML = ICONS[value];
        }
        else {
            this.element.classList.add("hidden");
            this.element.querySelector('.icon__icon').innerHTML = "";

        }
    }

    get icon () {
        return this._icon;
    }

    get iconSVG () {
        return ICONS[this._icon];
    }

    set stroke (value) {
        this._stroke = value;
        if (this._stroke !== undefined)
            this.element.querySelector('.icon__icon').classList.add('icon__icon--stroke');
        else
            this.element.querySelector('.icon__icon').classList.remove('icon__icon--stroke');
    }

    get stroke () {
        return this._stroke;
    }

}
