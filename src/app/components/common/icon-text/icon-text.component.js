import './icon-text.component.scss';
import './icon-text-mobile.component.scss';
import content from './icon-text.component.html';
import BaseUI from "../base/base-ui.component";

export default class IconTextComponent extends BaseUI {
    constructor (options) {
        var temp = document.createElement('div');
        temp.innerHTML = content;
        options.content = temp.innerHTML;
        super(options);
    }

    init(){
        this.text = this.data.text;
        this.icon = this.data.icon;
        this.stroke = this.data.stroke;
    }

    set text(value){
        this._text = value;
        this.element.querySelector('.icon-text__text').innerHTML = value;
    }
    get text(){
       return this._text;
    }

    set icon(value){
        this.$iconComponent.icon = value;
    }

    get icon(){
        return this.$iconComponent.icon;
    }

    set stroke(value){
        this.$iconComponent.stroke = value;
    }

    get stroke(){
        return this.$iconComponent.stroke;
    }
}
