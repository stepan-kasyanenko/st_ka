import './popup.directive.scss';
import BaseUI from '../base/base-ui.component';
import content from './popup.directive.html'

export default class PopupDirective extends BaseUI {
    constructor (options) {
        const temp = document.createElement('div');
        temp.innerHTML = content;
        const popupContent = temp.querySelector('.popup__content');
        popupContent.innerHTML = options.content || (options.element && options.element.innerHTML) || "";
        options.content = temp.innerHTML;
        super(options);
    }

    init () {
        this.placement = this.data.placement || "left";
        this.behavior = this.data.behavior || "click";
        this.$popup.classList.add(this.placement);

    }


    setBehaviorSubject (value) {
        if (value === this.behaviorSubject)
            return;
        this.behaviorSubject = value;
        if (this.behavior === "mousemove") {
            this.behaviorSubject.addEventListener("mouseenter", () => {
                this.show();
            });
            this.behaviorSubject.addEventListener("mouseleave", () => {
                this.hide();
            });
        }
    }

    show () {
        if(this.$popup.classList.contains("popup--active"))
            return;
        this.$popup.classList.add("popup--active");
        const that = this;
        if (this.behavior === "click") {
            document.addEventListener("mousedown", function hide (e) {
                const popupRect = that.$popup.getBoundingClientRect();
                if (e.clientX > popupRect.left && e.clientX < popupRect.left + popupRect.width && e.clientY > popupRect.top && e.clientY < popupRect.top + popupRect.height)
                    return;
                that.hide();
                document.removeEventListener("mousedown", hide, true);
            }, true);
        }
    }

    hide () {
        this.$popup.classList.remove("popup--active");
    }
}

