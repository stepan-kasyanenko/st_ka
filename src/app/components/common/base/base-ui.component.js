import {REGISTER} from "../../../module";

export default class BaseUI {
    constructor (options = {}) {
        let {parentNode, content} = options;
        this.components = [];
        const temp = document.createElement('div');
        temp.innerHTML = content;
        [].slice.call(temp.querySelectorAll('[data-id]')).forEach((elem) => {
            const id = elem.dataset.id;
            this["$" + id] = elem;
        });
        this.element = temp.firstChild;
        [].slice.call(temp.querySelectorAll('[st-click]:not([data-component])')).forEach((elem) => {
            elem.addEventListener("click", () => {
                const cl = this.__componentName || "data";
                const attr = elem.getAttribute('st-click');
                const func = new Function(cl, cl + "." + attr + "");
                func(this);
            });
        });
        parentNode.appendChild(temp.firstChild);
        if (this.element && this.element.querySelectorAll) {
            [].slice.call(this.element.querySelectorAll('[data-component]')).forEach(elem => {
                const name = elem.getAttribute('data-component');
                const newComponent = new REGISTER[name]({parentNode: elem.parentNode, element: elem});
                const id = elem.getAttribute("data-id");
                if (id) {
                    this["$" + id + "Component"] = newComponent;
                    newComponent.element.setAttribute("data-id", id);
                }
                newComponent.__componentName = name;
                const cssText = elem.style.cssText;
                if (cssText)
                    newComponent.element.style = cssText;
                [].slice.call(elem.classList).forEach((cl)=>newComponent.element.classList.add(cl));
                newComponent.data = elem.dataset;
                const click = elem.getAttribute('st-click');
                if (click) {
                    newComponent.element.addEventListener("click", () => {
                        const cl = this.__componentName || "data";
                        const func = new Function(cl, cl + "." + click + "");
                        func(this);
                    });
                }
                if (newComponent.init) {
                    newComponent.init();
                }
                this.components.push(newComponent);
                elem.parentNode.replaceChild(newComponent.element, elem);
            });
        }
    }
}