import './loader.component.scss';
import content from './loader.component.html';
import BaseUI from "../base/base-ui.component";

export default class LoaderComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        options.parentNode = document.body;
        super(options);
    }

}
let countInvoke = 0;
LoaderComponent.show = (parentNode = document.body) => {
    countInvoke++;
    const loader = document.querySelector('.loader');
    let classPos = parentNode === document.body ? "loader--show--fixed" : "loader--show--absolute";
    if (parentNode.isConnected || (parentNode.isConnected===undefined && document.body.contains(parentNode)))
        parentNode.appendChild(loader);
    loader.classList.add('loader--show');
    loader.classList.add(classPos);
};

LoaderComponent.hide = () => {
    countInvoke--;
    if (countInvoke === 0) {
        const loader = document.querySelector('.loader');
        loader.classList.remove('loader--show');
        loader.classList.remove("loader--show--fixed");
        loader.classList.remove("loader--show--absolute");
    }
};

