export class EventEmitterProperty {
    constructor (value) {
        this._value = value;
        this.onValueChange = new EventEmitter();
        if(this._value){
            this.onValueChange.next(this._value, undefined);
        }
    }

    set value (val) {
        const oldVal = this._value;
        this._value = val;
        this.onValueChange.next(val, oldVal);
    }

    get value () {
        return this._value;
    }
}

export class EventEmitter {
    constructor () {
        this.events = [];
    }

    then (func) {
        this.events.push(func);
    }

    next (val, old) {
        this.events.forEach(f => f(val, old));
    }
}