import './profile.component.scss';
import BaseUI from '../common/base/base-ui.component';
import content from './profile.component.html';

export default class ProfileComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
        this.createChildren();
    }

    createChildren () {

    }
}



