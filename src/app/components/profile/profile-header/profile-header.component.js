import './profile-header.component.scss';
import './profile-header-mobile.component.scss';
import content from './profile-header.component.html';
import TabContentComponent from "../../common/tabs/tab-content/tab-content.component";
import TabsComponent from "../../common/tabs/tabs.component";
import BaseUI from "../../common/base/base-ui.component";


export default class ProfileHeaderComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
    }

    init () {
        this.tabComponent = this.components.filter(f => f instanceof TabsComponent)[0];
        this.tabComponent.tabsService.current.onValueChange.then((n) => this.tabChanged(n));
        this.tabChanged(0);
    }

    tabChanged (val) {
        this.components.filter(f => f instanceof TabContentComponent)
          .forEach((comp, i) => {
              if (i === val)
                  comp.element.classList.add("tabs__tab__content--active");
              else
                  comp.element.classList.remove("tabs__tab__content--active");
          });
    }

    logout(){
        this.$popupComponent.show();
    }
}

