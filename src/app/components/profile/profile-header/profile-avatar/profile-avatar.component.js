import './profile-avatar.component.scss';
import './profile-avatar-mobile.component.scss';
import BaseUI from '../../../common/base/base-ui.component';
import content from './profile-avatar.component.html';
import image from '../../../../../assets/images/profile_image.jpg';


export default class ProfileAvatarComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
    }

    init () {
        // in normal app we receive it from API
        this.$image.style.backgroundImage = "url(" + image + ")";
    }
}
