import './profile-bio.component.scss';
import './profile-bio-mobile.component.scss';
import BaseUI from '../../../common/base/base-ui.component';
import content from './profile-bio.component.html';
import {ProfileAboutService} from "../../profile-about/profile-about.service";


export default class ProfileBioComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
    }

    init () {
        this.service = new ProfileAboutService();
        this.service.getProfile().then((model) => {
            this.model = model;
            this.nameChange(this.model.firstName.value.value);
            this.model.firstName.value.onValueChange.then((n) => this.nameChange(n));
            this.nameChange(this.model.lastName.value.value);
            this.model.lastName.value.onValueChange.then((n) => this.nameChange(n));

            this.addressChange(this.model.address.value.value);
            this.model.address.value.onValueChange.then((n) => this.addressChange(n));

            this.contactChange(this.model.contact.value.value);
            this.model.contact.value.onValueChange.then((n) => this.contactChange(n));
        });
    }

    nameChange () {
        this.$name.textContent = this.model.firstName.value.value + " " + this.model.lastName.value.value;
    }

    addressChange (val) {
        this.$addressComponent.text = val;
    }

    contactChange (val) {
        this.$contactComponent.text = val;
    }
}
