import {EventEmitterProperty} from "../../common/event-emitter-property/EventEmitterProperty";

export class ProfileAboutModel {
    constructor (firstName, lastName, website, contact, address) {
        this.firstName = {label: "First name", value: new EventEmitterProperty(firstName), icon: ""};
        this.lastName = {label: "Last name", value: new EventEmitterProperty(lastName), icon: ""};
        this.website = {label: "Website", value: new EventEmitterProperty(website), icon: "globe"};
        this.contact = {label: "Phone number", value: new EventEmitterProperty(contact), icon: "call"};
        this.address = {label: "City, state & zip", value: new EventEmitterProperty(address), icon: "home"};
    }

    get name(){
        return  this.firstName.value.value + " " + this.lastName.value.value;
    }

    getJSON () {
        return {
            firstName: this.firstName.value.value,
            lastName: this.lastName.value.value,
            website: this.website.value.value,
            contact: this.contact.value.value,
            address: this.address.value.value,
        }
    }
}