import './profile-about-edit.component.scss';
import './profile-about-edit-mobile.component.scss';
import BaseUI from '../../../common/base/base-ui.component';
import content from './profile-about-edit.component.html'
import {ProfileAboutService} from "../profile-about.service";
import LoaderComponent from "../../../common/loader/loader.component";
import {ProfileAboutModel} from "../profile-about.model";

export default class ProfileAboutEditComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
    }

    init () {
        this.service = new ProfileAboutService();
    }

    save () {
        LoaderComponent.show(this.element);
        const saveModel = new ProfileAboutModel(
          this.$firstNameComponent.value,
          this.$lastNameComponent.value,
          this.$websiteComponent.value,
          this.$contactComponent.value,
          this.$addressComponent.value,
        );
        this.service.putProfile(saveModel).then(() => {
            this.model.firstName.value.value = saveModel.firstName.value.value;
            this.model.lastName.value.value = saveModel.lastName.value.value;
            this.model.website.value.value = saveModel.website.value.value;
            this.model.contact.value.value = saveModel.contact.value.value;
            this.model.address.value.value = saveModel.address.value.value;
            this.hide();
            LoaderComponent.hide();
        });
    }

    cancel () {
        this.hide();
    }

    set model (val) {
        this._model = val;
        if (this._model)
            this.initProps(["firstName","lastName", "address", "contact", "website"]);
    }

    get model () {
        return this._model;
    }

    show (model) {
        this.element.classList.add("profile-about-edit--show");
        this.model = model;
    }

    hide () {
        this.element.classList.remove("profile-about-edit--show");
    }

    initProps (props) {
        props.forEach(f => {
            const component = this["$" + f + "Component"];
            const property = this._model[f];
            component.value = property.value.value;
            component.label = property.label;
        });
    }
}
