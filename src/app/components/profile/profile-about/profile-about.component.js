import './profile-about.component.scss';
import './profile-about-mobile.component.scss';
import BaseUI from '../../common/base/base-ui.component';
import content from './profile-about.component.html';
import {ProfileAboutService} from "./profile-about.service";
import LoaderComponent from "../../common/loader/loader.component";
import {ProfileAboutModel} from "./profile-about.model";


export default class ProfileAboutComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
    }

    init () {
        this.service = new ProfileAboutService();
        LoaderComponent.show();
        this.service.getProfile().then((model) => {
            this.model = model;
            this.initProps(["address", "contact", "website"]);
            this.initName();
            LoaderComponent.hide();
        });

    }

    initProps (props) {
        props.forEach(f => {
            const component = this["$" + f + "Component"];
            const property = this.model[f];
            component.text = property.value.value;
            component.icon = property.icon;
            component.label = property.label;
            component.onSaveEvent.then((value) => {
                LoaderComponent.show(this.element);
                this.service.putProperty(this.model, f, value).then(() => {
                    property.value.value = value;
                    component.text = value;
                    LoaderComponent.hide();
                });
            });
            property.value.onValueChange.then((n) => {
                component.text = n;
            });
        });
    }

    initName () {
        const component = this.$nameComponent;
        component.text = this.model.name;
        component.label = "First and Last name";
        component.onSaveEvent.then((value) => {
            const vals = value.split(" ");
            const firstName = vals[0] || "";
            const lastName = vals[1] || "";
            LoaderComponent.show(this.element);
            const saveModel = new ProfileAboutModel(
              firstName,
              lastName,
              this.$websiteComponent.value,
              this.$contactComponent.value,
              this.$addressComponent.value,
            );
            this.service.putProfile(saveModel).then(() => {
                this.model.firstName.value.value = firstName;
                this.model.lastName.value.value = lastName;
                component.text = this.model.name;
                LoaderComponent.hide();
            });
        });
        this.model.firstName.value.onValueChange.then(() => {
            component.text = this.model.name;
        });
        this.model.lastName.value.onValueChange.then(() => {
            component.text = this.model.name;
        });
    }

    editAbout () {
        this.$editComponent.show(this.model);
    }
}

