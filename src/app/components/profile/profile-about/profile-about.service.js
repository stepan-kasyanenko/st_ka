import {ProfileAboutModel} from "./profile-about.model";

let PROFILE = {
    firstName: "Jessica",
    lastName: "Parker",
    website: "wwww.seller.com",
    contact: "(949) 325 - 68694",
    address: "Newport Beach, CA",
};
let profilePromise = null;
let profile = null;

export class ProfileAboutService {

    getProfile () {
        if (profilePromise)
            return profilePromise;
        profilePromise = new Promise(resolve => {
            if (profile) {
                profilePromise = null;
                resolve(profile);
            } else {
                setTimeout(() => {
                    let storedData;
                    try {
                        storedData  = JSON.parse(localStorage.getItem("profile"));
                    }catch (e) {
                        storedData = {};
                    }
                    const data = Object.assign(PROFILE, storedData);
                    profile = new ProfileAboutModel(data.firstName, data.lastName, data.website, data.contact, data.address);
                    profilePromise = null;
                    resolve(profile);
                }, 0);
            }
        });
        return profilePromise;
    }

    putProfile (model) {
        return new Promise(resolve => {
            setTimeout(() => {
                localStorage.setItem("profile", JSON.stringify(model.getJSON()));
                resolve();
            }, 2000);
        })
    }

    putProperty (model, property, value) {
        return new Promise(resolve => {
            setTimeout(() => {
                const jsModel = model.getJSON();
                jsModel[property] = value;
                localStorage.setItem("profile", JSON.stringify(jsModel));
                resolve();
            }, 2000);
        })
    }
}