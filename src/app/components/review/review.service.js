export class ReviewService{
    constructor (reviewModel){
        this.model = reviewModel;
    }

    changeTotal(total){
        this.model.total.value =total;
    }

    changeCurrent(current){
        this.model.current.value = current;
    }
}