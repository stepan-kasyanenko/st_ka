import {EventEmitterProperty} from "../common/event-emitter-property/EventEmitterProperty";

export class ReviewModel{
    constructor (){
        this.total = new EventEmitterProperty(6);
        this.current = new EventEmitterProperty(3);
    }
}