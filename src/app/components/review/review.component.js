import './review.component.scss';
import './review-mobile.component.scss';
import BaseUI from '../common/base/base-ui.component';
import content from './review.component.html';
import {ReviewModel} from "./review.model";
import {ReviewService} from "./review.service";
import IconComponent from "../common/icon/icon.component";


export default class ReviewComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
    }

    init () {
        this.model = new ReviewModel();
        this.service = new ReviewService(this.model);
        this.model.current.onValueChange.then(() => this.showCurrent());
        this.model.total.onValueChange.then(() => this.showTotal());
        this.showCurrent();
        this.showTotal();
    }

    setCurrent (cnt) {
        this.service.changeCurrent(cnt);
        this.service.changeTotal(Math.round(Math.random() * 20)); //just for test
    }

    showCurrent () {
        this.components.filter(f => f instanceof IconComponent).forEach((icon, i) => {
            if (i < this.model.current.value)
                icon.stroke = undefined;
            else
                icon.stroke = true;
        });
    }

    showTotal () {
        this.$total.textContent = this.model.total.value;
        if (+this.model.total.value === 0)
            this.$text.textContent = "Review";
        else
            this.$text.textContent = "Reviews";
    }
}

