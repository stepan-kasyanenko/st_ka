import './follow.component.scss';
import './follow-mobile.component.scss';
import BaseUI from '../common/base/base-ui.component';
import content from './follow.component.html';
import {FollowModel} from "./follow.model";
import {FollowService} from "./follow.service";


export default class FollowComponent extends BaseUI {
    constructor (options) {
        options.content = content;
        super(options);
    }

    init(){
        this.model = new FollowModel();
        this.service = new FollowService(this.model);
        this.model.total.onValueChange.then(() => this.showTotal());
        this.showTotal();
        this.$popupComponent.setBehaviorSubject(this.$follow);
    }

    changeTotal(){
        this.service.changeTotal(this.model.total.value+1);
    }

    showTotal () {
        this.$total.textContent = this.model.total.value;
        if (+this.model.total.value === 0)
            this.$text.textContent = "Follower";
        else
            this.$text.textContent = "Followers";
    }
}

